package MyFinalProject.controller;

import MyFinalProject.mapper.EmergencyMapper;
import MyFinalProject.model.Emergency;
import MyFinalProject.model.EmergencyDto;
import MyFinalProject.model.Victim;
import MyFinalProject.model.VictimDto;
import MyFinalProject.service.EmergencyService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/emergency")
public class EmergencyController {
@Autowired
    EmergencyService emergencyService;
@Autowired
    EmergencyMapper emergencyMapper;


    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponse(code = 200, message = "Jednostka pomocnicza utworzona")
    @ApiOperation(value = "Tworzy jednostke pomocnicza", response = Long.class)
    public ResponseEntity <Long> createEmergency(@ApiParam("Tworzona jednostka pomocnicza") @RequestBody EmergencyDto emergencyDto) {
        Emergency createdEmergency = emergencyService.addEmergency(emergencyMapper.map(emergencyDto));
        return ResponseEntity.ok(createdEmergency.getId());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Jednostka zostala edytowana"),
            @ApiResponse(code = 404, message = "Jednostka pomocnicza o podanym id nie istnieje")
    })
    @ApiOperation(value = "Edytuje jednostke pomocnicza", response = Void.class)
    public ResponseEntity<Void> editEmergency(@ApiParam("Edytowana jednostka pomocnicza") @RequestBody EmergencyDto emergencyDto, @ApiParam("Id jednostki pomocniczej") @PathVariable("id") Long id) {
        Emergency updatedEmergency =emergencyService.editEmergency(emergencyMapper.map(emergencyDto),id);
        if (updatedEmergency == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().build();
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({@ApiResponse(code = 200, message = "Jednostka pomocnicza zostala pobrana"),
            @ApiResponse(code = 404, message = "Jednostka pomocnicza nie istnieje")})
    @ApiOperation(value = "Pobiera jednostke pomocnicza", response = VictimDto.class)
    public ResponseEntity<EmergencyDto> getEmergency(@ApiParam("Id jednostki pomocniczej") @PathVariable("id") Long id) {
        Emergency emergency = emergencyService.getById(id);
        if (emergency == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(emergencyMapper.map(emergency));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiResponse(code = 204, message = "Jednostka pomocnicza zostala usunieta")
    @ApiOperation(value = "Usuwa jednostke pomocnicza", response = Void.class)
    public ResponseEntity<Void> deleteEmergency(@ApiParam("Id jednostki pomocniczej") @PathVariable("id") Long id) {
        emergencyService.deleteEmergency(id);
        return ResponseEntity.noContent().build();
    }

}

