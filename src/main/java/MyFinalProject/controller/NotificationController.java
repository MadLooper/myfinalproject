package MyFinalProject.controller;

import MyFinalProject.mapper.NotificationMapper;
import MyFinalProject.model.Notification;
import MyFinalProject.model.NotificationDto;
import MyFinalProject.service.NotificationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/notification")
public class NotificationController {
    @Autowired
    NotificationService notificationService;
    @Autowired
    NotificationMapper notificationMapper;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponse(code = 200, message = "Notyfikacja zostala utworzona")
    @ApiOperation(value = "Tworzy notyfikacje", response = Long.class)
    public ResponseEntity<Long> createNotification(@ApiParam("Tworzona notyfikacja") @RequestBody NotificationDto notificationDto) {
        Notification createdNotification = notificationService.addNotification(notificationMapper.map(notificationDto));
        return ResponseEntity.ok(createdNotification.getId());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Notyfikacja zostala edytowana"),
            @ApiResponse(code = 404, message = "Notyfikacja o podanym id nie istnieje")
    })
    @ApiOperation(value = "Edytuje notyfikacje", response = Void.class)
    public ResponseEntity<Void> editNotification(@ApiParam("Edytowana notyfikacja") @RequestBody NotificationDto notificationDto, @ApiParam("Id notyfikacji") @PathVariable("id") Long id) {
        Boolean updatedNotification = notificationService.editNotification(notificationMapper.map(notificationDto), id);
        if (updatedNotification == false) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Notyfikacja zostala pobrana"),
            @ApiResponse(code = 404, message = "Notyfikacja nie istnieje")})
    @ApiOperation(value = "Pobiera notyfikacje", response = NotificationDto.class)
    public ResponseEntity<NotificationDto> getNotification(@ApiParam("Id notyfikacji") @PathVariable("id") Long id) {
        Notification notification = notificationService.getById(id);
        if (notification == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(notificationMapper.map(notification));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiResponse(code = 204, message = "Notyfikacja zostala usunieta")
    @ApiOperation(value = "Usuwa notyfikacje", response = Void.class)
    public ResponseEntity<Void> deleteNotification(@ApiParam("Id notyfikacji") @PathVariable("id") Long id) {
        notificationService.deleteNotification(id);
        return ResponseEntity.noContent().build();
    }
}
