package MyFinalProject.controller;

import MyFinalProject.mapper.SimpleNotificationMapper;
import MyFinalProject.model.SimpleNotificationDto;
import MyFinalProject.service.NotificationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/simpleNotifications")

public class SimpleNotificationController {
    @Autowired
   NotificationService notificationService;
    @Autowired
    SimpleNotificationMapper simpleNotificationMapper;

    @RequestMapping(value = "/simpleNotification", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Zapisuje lokalizacje.", notes = "", response = Void.class)
    @ApiResponse(code = 200, message = "User zostal zapisany", response = Void.class)
    public ResponseEntity<String> addLocation(@ApiParam(value = "Zapisywana lokalizacja") @RequestBody SimpleNotificationDto simpleNotificationDto) {
        notificationService.addNotification(simpleNotificationMapper.map(simpleNotificationDto));
        return new ResponseEntity<String>("Dodano do bazy danych", HttpStatus.OK);
    }
    }
