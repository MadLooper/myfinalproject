////package MyFinalProject.controller;
////przygotowana na potem
////import MyFinalProject.mapper.NotificationMapper;
////import MyFinalProject.model.Notification;
////import MyFinalProject.model.NotificationDto;
////import MyFinalProject.model.TypeOfAccident;
////import MyFinalProject.repository.NotificationRepository;
////import MyFinalProject.service.NotificationService;
////import io.swagger.annotations.ApiOperation;
////import io.swagger.annotations.ApiParam;
////import io.swagger.annotations.ApiResponse;
////import io.swagger.annotations.ApiResponses;
////import org.springframework.beans.factory.annotation.Autowired;
////import org.springframework.format.annotation.DateTimeFormat;
////import org.springframework.http.MediaType;
////import org.springframework.http.ResponseEntity;
////import org.springframework.web.bind.annotation.*;
////
////import java.util.Date;
////import java.util.List;
////
////
////@RestController
////@RequestMapping(value = "/report")
////public class StatisticsController {
////
////    //1. ilosc pozycji w miesiacu do ilosci pozycji miesiac temu, do ilosci pozycji last year
////    //2.Starts with 44.4444...&& 33.333... ile razy w danym miesiaca zostalo dokonane przestepstwo w danej arei
////    //3. Podsumuj ile przestepstw wzgledem typu w danym miesiacu, miesiac temu, last year
////    //4. sredni czas dojechania do ofiary
////    @Autowired
////    private NotificationService notificationService;
////    @Autowired
////    private NotificationMapper notificationMapper;
////    @Autowired
////    private NotificationRepository notificationRepository;
////
////
////    //Drukuje ilosc ofiar w ustawionym zakresie dat.
////    @RequestMapping(value = "/numberOfAllVictims", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
////    @ApiResponse(code = 200, message = "Obliczenia udane")
////    @ApiOperation(value = "Liczy ile bylo wszystkich wypadkow w danym miesiacu", response = Integer.class)
////    public ResponseEntity<Integer> getAllNotifications(@ApiParam("Tworzona notyfikacja") @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date date1,@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd")Date date2) {
////        System.out.println(getAllNotification());
////        return ResponseEntity.ok(notificationRepository.numberOfAllVictims(date1, date2));
////    }
////
////    //wyswietla wszystkie notyfikacje w danym zakresie dat.
////    @GetMapping(path = "/all")
////    public @ResponseBody
////    Iterable<Notification> getAllNotification() {
////        return notificationRepository.findAll();
////    }
////
////    @RequestMapping(value = "/findByType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
////    @ApiOperation(value = "Pobiera notyfikacje po typie", response = NotificationDto.class)
////    @ApiResponses(value = {@ApiResponse(code = 200, message = "Notyfikacja pobrana poprawnie", response = NotificationDto.class),
////            @ApiResponse(code = 404, message = "Nie znaleziono notyfikacji", response = NotificationDto.class)})
////    public ResponseEntity<List<Notification>> findByTypeandDate (@ApiParam(value = "Nazwa pobieranej notyfikacji")@RequestParam("type") TypeOfAccident type) {
////        return ResponseEntity.ok(notificationRepository.findByType(type));
////
////    }
//
////    @RequestMapping(value = "/{arrivalTimeLongerThanTenMin}", method = RequestMethod.GET)
////    @ApiOperation(value = "Pobiera notyfikacje po czasie dojazdu", notes = "", response = NotificationDto.class)
////    @ApiResponses(value = { @ApiResponse(code = 200, message = "Notyfikacja pobrana poprawnie", response = NotificationDto.class),
////            @ApiResponse(code = 404, message = "Nie znaleziono notyfikacji", response = NotificationDto.class) })
////    public ResponseEntity<List<Notification>> getNotification(@ApiParam(value = "Nazwa pobieranej notyfikacji") @RequestParam("time") double time) {
////        List<Notification> notifications = notificationRepository.arrivalTimeLongerThanTenMin();
////        System.out.println(notifications);
////        return new ResponseEntity<>(notifications, HttpStatus.OK);
////    }
//}
//
//
////    @GetMapping(path="/kk")
////    public @ResponseBody Integer getAllUsers() {
////        // This returns a JSON or XML with the users
////        return notificationRepository.numberOfVictimByType();
////    }
//
////    @RequestMapping(value = "/listOfArrivalTime", method = RequestMethod.GET)
////    @ApiResponse(code = 200, message = "Obliczenia udane")
////    @ApiOperation(value = "Pokazuje liste jak szybko pomoc dojezdzala do celu ogolem", response = Integer.class)
////    public ResponseEntity<Integer> getNotification(@ApiParam("Tworzona notyfikacja") @RequestParam double time, LocalDate date) {
////        return ResponseEntity.ok(notificationService.listOfArrivalTime());
////    }
////    @RequestMapping(value = "/arrivalTimeByType", method = RequestMethod.GET)
////    @ApiResponse(code = 200, message = "Obliczenia udane")
////    @ApiOperation(value = "Liczy jak szybko pomoc dojechala do celu w danym typie", response = Integer.class)
////    public ResponseEntity<Integer> getNotification(@ApiParam("Tworzona notyfikacja") @RequestParam TypeOfAccident type, LocalDate date) {
////        return ResponseEntity.ok(notificationService.numberOfVictimByType());
////    }
////
////    @RequestMapping(value = "/averageArrivalTime", method = RequestMethod.GET)
////    @ApiResponse(code = 200, message = "Obliczenia udane")
////    @ApiOperation(value = "Liczy sredni czas dojazdu do celu dla wszystkich typow", response = Integer.class)
////    public ResponseEntity<Integer> getNotification(@ApiParam("Tworzona notyfikacja") @RequestParam TypeOfAccident type, LocalDate date) {
////        return ResponseEntity.ok(notificationService.numberOfVictimByType());
////    }
////    @RequestMapping(value = "/averageArrivalTimeByType", method = RequestMethod.GET)
////    @ApiResponse(code = 200, message = "Obliczenia udane")
////    @ApiOperation(value = "Liczy sredni czas dotarcia do celu w danym typie", response = Integer.class)
////    public ResponseEntity<Integer> getNotification(@ApiParam("Tworzona notyfikacja") @RequestParam TypeOfAccident type, LocalDate date) {
////        return ResponseEntity.ok(notificationService.numberOfVictimByType());
////    }
////
////    @RequestMapping(value = "/numberOfVictimByTypeOverTheYears", method = RequestMethod.GET)
////    @ApiResponse(code = 200, message = "Obliczenia udane")
////    @ApiOperation(value = "Liczy ilosc ofiar na przestrzeni lat w danym typie", response = Integer.class)
////    public ResponseEntity<Integer> getNotification(@ApiParam("Tworzona notyfikacja") @RequestParam TypeOfAccident type, LocalDate date) {
////        return ResponseEntity.ok(notificationService.numberOfVictimByType());
////    }
////    @RequestMapping(value = "/averageArrivalTimeByTypeOverTheYears", method = RequestMethod.GET)
////    @ApiResponse(code = 200, message = "Obliczenia udane")
////    @ApiOperation(value = "Liczy sredni czas dojechania do celu w danym typie na przestrzeni lat", response = Integer.class)
////    public ResponseEntity<Integer> getNotification(@ApiParam("Tworzona notyfikacja") @RequestParam TypeOfAccident type, LocalDate date) {
////        return ResponseEntity.ok(notificationService.numberOfVictimByType());
////    }
////
////    @RequestMapping(value = "/howManyCrimesInThisArea", method = RequestMethod.GET)
////    @ApiResponse(code = 200, message = "Obliczenia udane")
////    @ApiOperation(value = "Liczy ile przestepstw dokonano w danej okolicy", response = Integer.class)
////    public ResponseEntity<Integer> getNotification(@ApiParam("Tworzona notyfikacja") @RequestParam TypeOfAccident type, LocalDate date) {
////        return ResponseEntity.ok(notificationService.numberOfVictimByType());
////    }
////
////
////    @RequestMapping(value = "/betterOrWorseToLastYearToLastMonthByType", method = RequestMethod.GET)
////    @ApiResponse(code = 200, message = "Obliczenia udane")
////    @ApiOperation(value = "Liczy procentowy wspolczynnik ilosci zgloszen w danym typie wzgl. LY oraz LM ", response = Integer.class)
////    public ResponseEntity<Integer> getNotification(@ApiParam("Tworzona notyfikacja") @RequestParam TypeOfAccident type, LocalDate date) {
////        return ResponseEntity.ok(notificationService.numberOfVictimByType());
////    }
////
////    @RequestMapping(value = "/betterOrWorseTimeToLastYearToLastMonthByType", method = RequestMethod.GET)
////    @ApiResponse(code = 200, message = "Obliczenia udane")
////    @ApiOperation(value = "Liczy procentowy wspolczynnik czasu dojazdu w danym typie wzgl. LY oraz LM", response = Integer.class)
////    public ResponseEntity<Integer> getNotification(@ApiParam("Tworzona notyfikacja") @RequestParam TypeOfAccident type, LocalDate date) {
////        return ResponseEntity.ok(notificationService.numberOfVictimByType());
////    }
//
//
//
