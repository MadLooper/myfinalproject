package MyFinalProject.controller;

import MyFinalProject.mapper.VictimMapper;
import MyFinalProject.model.Victim;
import MyFinalProject.model.VictimDto;
import MyFinalProject.service.VictimService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/victim")
public class VictimController {

    @Autowired
    private VictimService victimService;
    @Autowired
    private VictimMapper victimMapper;


    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponse(code = 200, message = "Ofiara utworzona")
    @ApiOperation(value = "Tworzy ofiare", response = Long.class)
    public ResponseEntity<Long> createVictim(@ApiParam("Tworzona ofiara") @RequestBody VictimDto victimDto) {
        Victim createdVictim = victimService.addVictim(victimMapper.map(victimDto));
        return ResponseEntity.ok(createdVictim.getId());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ofiara zostala edytowana"),
            @ApiResponse(code = 404, message = "Ofiara nie istnieje")
    })
    @ApiOperation(value = "Edytuje ofiare", response = Void.class)
    public ResponseEntity<Void> editVictim(@ApiParam("Edytowana ofiara") @RequestBody VictimDto victimDto, @ApiParam("Id ofiary") @PathVariable("id") Long id) {
        Victim updatedVictim = victimService.editVictim((victimMapper.map(victimDto)), id);
        if (updatedVictim == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({@ApiResponse(code = 200, message = "Ofiara zostala pobrana"),
            @ApiResponse(code = 404, message = "Ofiara nie istnieje")})
    @ApiOperation(value = "Pobiera ofiare", response = VictimDto.class)
    public ResponseEntity<VictimDto> getVictim(@ApiParam("Id ofiary") @PathVariable("id") Long id) {
        Victim victim = victimService.getById(id);
        if (victim == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(victimMapper.map(victim));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiResponse(code = 204, message = "Ofiara zostala usunieta")
    @ApiOperation(value = "Usuwa ofiare", response = Void.class)
    public ResponseEntity<Void> deleteVictim(@ApiParam("Id ofiary") @PathVariable("id") Long id) {
        victimService.deleteVictim(id);
        return ResponseEntity.noContent().build();
    }


}
