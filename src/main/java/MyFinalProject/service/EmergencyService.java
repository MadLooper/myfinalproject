package MyFinalProject.service;

import MyFinalProject.model.Emergency;
import MyFinalProject.repository.EmergencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmergencyService {

    @Autowired
    private EmergencyRepository emergencyRepo;

    //umozliwia dodanie jednostki pomocniczej
    public Emergency addEmergency(Emergency emergency) {
        return emergencyRepo.save(emergency);
    }

    //umozliwia edycje danych jednostki pomocniczej
    public Emergency editEmergency(Emergency emergency, Long id) {
        Emergency original = emergencyRepo.findOne(id);
        if (original != null) {
            original.setName(emergency.getName());
            original.setGeoHeight(emergency.getGeoHeight());
            original.setGeoWidth(emergency.getGeoWidth());
            original.setBusy(emergency.isBusy());
            original.setId(emergency.getId());
           return emergencyRepo.save(original);
        }
        return null;
    }

    //Umozliwia pobranie danych jednostki pomocniczej
    public Emergency getById (Long id){
        return emergencyRepo.findOne(id);
    }

    //umozliwia usuniecie ofiary
    public void deleteEmergency (Long id){
        Emergency emergency= emergencyRepo.findOne(id);
        if (emergency!=null){
            emergencyRepo.delete(id);
        }
    }
}
