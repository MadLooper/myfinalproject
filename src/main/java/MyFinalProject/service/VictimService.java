package MyFinalProject.service;

import MyFinalProject.model.Victim;
import MyFinalProject.repository.VictimRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VictimService {

    @Autowired
    private VictimRepository victimRepo;

//Tworzy nowa ofiare

    public Victim addVictim(Victim victim) {
        return victimRepo.save(victim);
    }

    //umozliwia edycje danych ofiary
    public Victim editVictim(Victim victim, Long id) {
        Victim original = victimRepo.findOne(id);
        if (original != null){
            original.setName(victim.getName());
            original.setPhoneNumber(victim.getPhoneNumber());
            original.setGeoWidth(victim.getGeoWidth());
            original.setGeoHeight(victim.getGeoWidth());
            original.setSurname(victim.getSurname());
            original.setSocialSecurityNumber(victim.getSocialSecurityNumber());
            original.setAddress(victim.getAddress());
            return victimRepo.save(original);
        }
        else return null;
    }
    //Umozliwia pobranie danych ofiary
    public Victim getById (Long id){
        return victimRepo.findOne(id);
    }

//umozliwia usuniecie ofiary
public void deleteVictim (Long id){
        Victim victim1 = victimRepo.findOne(id);
        if (victim1!=null){
            victimRepo.delete(id);
        }
}
}
