package MyFinalProject.service;

import MyFinalProject.model.Notification;
import MyFinalProject.model.NotificationDto;
import MyFinalProject.model.TypeOfAccident;
import MyFinalProject.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public class NotificationService {

    @Autowired
    NotificationRepository notificationRepo;


    //umozliwia dodanie notyfikacji
    public Notification addNotification(Notification notification) {
        return notificationRepo.save(notification);
    }
    //umozliwia edycje notyfikacji

    public boolean editNotification(Notification notification, Long id) {
        Notification original = notificationRepo.findOne(id);
        if (original != null) {
            original.setName(notification.getName());
            original.setInProgress(notification.isInProgress());
            original.setCancelled(notification.isCancelled());
            original.setDescription(notification.getDescription());
            original.setType(notification.getType());
            original.setLatitude(notification.getLatitude());
            original.setLongitude(notification.getLongitude());
            original.setDate(notification.getDate());
            notificationRepo.save(original);
            return true;
        }
        return false;
    }

    //umozliwia pobranie notyfikacji
    public Notification getById(Long id) {
        return notificationRepo.findOne(id);
    }

    //umozliwia usuniecie  notyfikacji
    public void deleteNotification(Long id) {
        Notification notification = notificationRepo.findOne(id);
        if (notification != null) {
            notificationRepo.delete(id);
        }
    }

    public List<Notification> getByType(TypeOfAccident typeOfAccident) {
        return (List<Notification>) this.notificationRepo.findByType(typeOfAccident);
    }
}
//        public double listOfArrivalTime() {
//        return notificationRepo.listOfArrivalTime();
//    }

//    public int numberOfAllVictims() {
//        return notificationRepo.numberOfAllVictims();
//    }


//    public int numberOfVictimByType() {
//        return notificationRepo.numberOfVictimByType();
//    }

//    public double listOfArrivalTime() {
//        return notificationRepo.listOfArrivalTime();
//    }
