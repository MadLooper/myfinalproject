package MyFinalProject.repository;

import MyFinalProject.model.Notification;
import MyFinalProject.model.NotificationDto;
import MyFinalProject.model.TypeOfAccident;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
//implementuje repozytorium JPA, ktore posiada wszystkie metody CRUD

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {

    Notification findByType(TypeOfAccident typeOfAccident);
//
//    @Query(value = "SELECT n from Notification n where n.cancelled=false AND n.inProgress=false ")
//    List<Notification>  findNewNotification();
////
    //przydatne pozniej do modulu analiz
//    @Query("SELECT COUNT(n) FROM Notification n where n.date BETWEEN :#{#date1} AND :#{#date2}")
//    int numberOfAllVictims(Date date1, Date date2);
//
//    @Query(value = "SELECT n from Notification n where n.type=:#{#type}")
//    List<Notification> findByType(@Param("type") TypeOfAccident type2);
//
//    @Query("select count (n) from Notification n where n.time>10")
//    int arrivalTimeLongerThanTenMin();

//    @Query("SELECT COUNT(n) FROM Notification n where size(n.date) n.date =:#{#notification.date} BETWEEN current_date")
//    List<Notification> printAllVictims(@Param("notification") Notification notification);


}
