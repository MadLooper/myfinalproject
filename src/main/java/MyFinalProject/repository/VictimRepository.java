package MyFinalProject.repository;

import MyFinalProject.model.Victim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
//implementuje repozytorium JPA, ktore posiada wszystkie metody CRUD

@Repository
public interface VictimRepository extends JpaRepository<Victim, Long>{
    Victim findByName(String name);
    Victim findBySocialSecurityNumber(String number);
}
