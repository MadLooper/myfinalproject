package MyFinalProject.repository;

import MyFinalProject.model.Emergency;
import MyFinalProject.model.Victim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
//implementuje repozytorium JPA, ktore posiada wszystkie metody CRUD
public interface EmergencyRepository extends JpaRepository<Emergency, Long> {
    Emergency findByName(String name);
}
