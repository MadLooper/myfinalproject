package MyFinalProject.JSPController;

import MyFinalProject.mapper.NotificationMapper;
import MyFinalProject.model.Notification;
import MyFinalProject.model.NotificationDto;
import MyFinalProject.service.NotificationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class AddNotificationController {
    @RequestMapping("/addNotificationForm")
    public String action(@RequestParam(value="lat", required=false) String lat,
                         @RequestParam(value="lng", required = false) String lng, Model model) {
        model.addAttribute("lat", lat);
        model.addAttribute("lng", lng);
        return "addNotificationForm";
    }
    @Autowired
    NotificationService notificationService;
    @Autowired
    NotificationMapper notificationMapper;

    @RequestMapping(params="!id", value = "/dispatcher", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ApiResponse(code = 200, message = "Notyfikacja zostala utworzona")
    @ApiOperation(value = "Tworzy notyfikacje", response = NotificationDto.class)
    public String createNotification(@ApiParam("Tworzona notyfikacja") @ModelAttribute NotificationDto notificationDto) {
        Notification createdNotification = notificationService.addNotification(notificationMapper.map(notificationDto));
        return "addNotificationForm";
    }
}