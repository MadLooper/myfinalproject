package MyFinalProject.JSPController;

import MyFinalProject.mapper.VictimMapper;
import MyFinalProject.model.Victim;
import MyFinalProject.model.VictimDto;
import MyFinalProject.service.VictimService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@Controller
public class VictimDetailController {
    @RequestMapping("/victimDetails")
    public String action(Map<String, Object> model){
        //    model.put("geoWidth",23);
        return "VictimDetails";
    }
    @Autowired
    VictimService victimService;
    @Autowired
    VictimMapper victimMapper;


    @RequestMapping(params="!id", value = "/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ApiResponse(code = 200, message = "Ofiara utworzona")
    @ApiOperation(value = "Tworzy ofiare", response = VictimDto.class)
    public String createVictim(@ApiParam("Tworzona ofiara") @ModelAttribute VictimDto victimDto) {
        Victim createdVictim = victimService.addVictim(victimMapper.map(victimDto));
        return "VictimDetails";
    }
}
