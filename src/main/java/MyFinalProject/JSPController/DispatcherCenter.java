package MyFinalProject.JSPController;

import MyFinalProject.mapper.NotificationMapper;
import MyFinalProject.model.Notification;
import MyFinalProject.model.NotificationDto;
import MyFinalProject.repository.NotificationRepository;
import MyFinalProject.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Controller
public class DispatcherCenter{
    @RequestMapping("/dispatcher")
    public String dispatcher(Map<String, Object> model){
        //    model.put("geoWidth",23);
        return "dispatcher";
    }

    //przygotowanie do podopiecia bazy danych pod strone jsp
//    @Autowired
//    private NotificationRepository notificationRepository;
//    @Autowired
//    private NotificationService notificationService;
//    @Autowired
//    NotificationMapper notificationMapper
//
//    @RequestMapping(value = "entities", method = RequestMethod.GET)
//    public ModelAndView showNotifications(HttpServletRequest request, HttpServletResponse response, Notification id){
//        List<Notification> notifications = notificationService.findNewNotifications.getNotification(id);
//        ModelAndView mav = new ModelAndView("notifications");
//        mav.addObject("notifications", notifications);
//        return mav;
//
//
//    }
}
