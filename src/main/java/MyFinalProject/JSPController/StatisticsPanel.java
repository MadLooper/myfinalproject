package MyFinalProject.JSPController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class StatisticsPanel {
    @RequestMapping("/statisticsPanel")
    public String action(Map<String, Object> model){
        //    model.put("geoWidth",23);
        return "statisticsPanel";
    }
}
