package MyFinalProject.JSPController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class ActionPanel {

    @RequestMapping(value = "/actionpanel")
    public String handleRequest(@RequestParam(value="lat", required=false) String lat,
    @RequestParam(value="lng", required = false) String lng, Model model) {
        model.addAttribute("lat", lat);
        model.addAttribute("lng", lng);
        return "action";
    }
}