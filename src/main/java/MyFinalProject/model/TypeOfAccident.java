package MyFinalProject.model;

public enum TypeOfAccident {
    ROAD_ACCIDENT, CRIMES_AND_MURDERS, MINOR_OFFENCES, HEALTH_ACCIDENT;
    //ROAD_ACCIDENT - police
    //CRIMES_AND_MURDERS - police and ambulance
    //MINOR_OFFENCES - police
    // HEALTH_ACCIDENT - ambulance
}
