package MyFinalProject.mapper;

import MyFinalProject.model.Emergency;
import MyFinalProject.model.EmergencyDto;
import org.springframework.stereotype.Component;

@Component
public class EmergencyMapper {

    public Emergency map (EmergencyDto emergencyDto){
        Emergency entity = new Emergency();
        entity.setId(emergencyDto.getId());
        entity.setName(emergencyDto.getName());
        entity.setGeoWidth(emergencyDto.getGeoWidth());
        entity.setGeoHeight(emergencyDto.getGeoHeight());
        entity.setBusy(emergencyDto.isBusy());
        return entity;
    }
    public EmergencyDto map (Emergency entity){
        EmergencyDto dto = new EmergencyDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setGeoWidth(entity.getGeoWidth());
        dto.setGeoHeight(entity.getGeoHeight());
        dto.setBusy(entity.isBusy());
        return dto;
    }
}
