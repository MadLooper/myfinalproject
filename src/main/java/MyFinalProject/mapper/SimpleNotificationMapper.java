package MyFinalProject.mapper;

import MyFinalProject.model.Notification;
import MyFinalProject.model.SimpleNotificationDto;
import org.springframework.stereotype.Component;

@Component
public class SimpleNotificationMapper {

    public Notification map (SimpleNotificationDto simpleNotificationDto){
        Notification entity = new Notification();
        entity.setId(simpleNotificationDto.getId());
        entity.setLatitude(simpleNotificationDto.getLatitude());
        entity.setLongitude(simpleNotificationDto.getLongitude());
       return entity;
    }

    public SimpleNotificationDto map (Notification notification){
        SimpleNotificationDto dto = new SimpleNotificationDto();
        dto.setId(notification.getId());
        dto.setLatitude(notification.getLatitude());
        dto.setLongitude(notification.getLongitude());
        return dto;
    }
}
