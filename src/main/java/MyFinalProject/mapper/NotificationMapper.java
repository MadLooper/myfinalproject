package MyFinalProject.mapper;

import MyFinalProject.model.Notification;
import MyFinalProject.model.NotificationDto;
import org.springframework.stereotype.Component;

@Component
public class NotificationMapper {

    public Notification map (NotificationDto notificationDto){
        Notification entity = new Notification();
        entity.setDescription(notificationDto.getDescription());
        entity.setType(notificationDto.getType());
        entity.setCancelled(notificationDto.isCancelled());
        entity.setId(notificationDto.getId());
        entity.setInProgress(notificationDto.isInProgress());
        entity.setName(notificationDto.getName());
        entity.setLatitude(notificationDto.getLatitude());
        entity.setLongitude(notificationDto.getLongitude());
        entity.setDate(notificationDto.getDate());
        return entity;
    }

    public NotificationDto map (Notification notification){
        NotificationDto dto = new NotificationDto();
        dto.setDescription(notification.getDescription());
        dto.setCancelled(notification.isCancelled());
        dto.setId(notification.getId());
        dto.setInProgress(notification.isInProgress());
        dto.setName(notification.getName());
        dto.setType(notification.getType());
        dto.setLatitude(notification.getLatitude());
        dto.setLongitude(notification.getLongitude());
        dto.setDate(notification.getDate());
        return dto;
    }
}
