package MyFinalProject.mapper;

import MyFinalProject.model.Victim;
import MyFinalProject.model.VictimDto;
import org.springframework.stereotype.Component;

@Component
public class VictimMapper {

    public Victim map(VictimDto dto){
        Victim entity = new Victim();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setGeoHeight(dto.getGeoHeight());
        entity.setGeoWidth(dto.getGeoWidth());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setAddress(dto.getAddress());
        entity.setSocialSecurityNumber(dto.getSocialSecurityNumber());
        entity.setSurname(dto.getSurname());
        return entity;
    }

    public VictimDto map (Victim entity){
        VictimDto dto = new VictimDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setGeoHeight(entity.getGeoHeight());
        dto.setGeoWidth(entity.getGeoWidth());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setAddress(entity.getAddress());
        dto.setSocialSecurityNumber(dto.getSocialSecurityNumber());
        dto.setSurname(dto.getSurname());
        return dto;
    }
}
