<%--
  Created by IntelliJ IDEA.
  User: KW
  Date: 10/30/2017
  Time: 12:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <html>
    <head>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <meta charset="utf-8">
        <title>Traffic layer</title>
        <style>
            /* Mapa zawsze musi miec zdefiniowany rozmiar */
            #map {
                height: 100%;
            }

            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
            }

            /*przycisk rozpoczynajacy akcje geolokacji, wyznaczajacy trase do ofiary*/
            #Button {
                background-color: whitesmoke;
                color: black;
                width: 120px;
                padding: 10px 15px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 12px;
                margin: 4px 2px;
                cursor: pointer;
                box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

            }

            /*ksztalt i rozmiar czcionki*/
            #getValue {
                font-size: 20px;
                font-family: "Roboto", helvetica, arial, sans-serif;
                font-weight: 100;
                line-height: 0.2;
            }
        </style>
    </head>
<body>
<script>
    TODO: Dokoncz

    //funkcja pobierajaca lokalizacje karetki jako punkt A przy wyznaczaniu trasy
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    }

    function showPosition(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        map.setCenter(new google.maps.LatLng(lat, lng));
    }
</script>
<form action="http://maps.google.com/maps" id="getValue" method="get" target="_blank">
    <%--//przypisuje punkowi poczatkowemu A lokalizacje karetki, punktowi B - lokalizacje ofiary pobrana z paska URL.--%>
    <%--Istnieje mozliwosc wpisania danych recznie w okienku--%>
    Enter address to your purpose:
    <input type="hidden" name="daddr" value="${navigator.geolocation.getCurrentPosition(showMap)}"/>
    <input type="text" name="saddr" value=" ${lat}, ${lng}"/>
    <input type="submit" id="Button" value="get directions"/>
</form>

<%--Wyswietla mape obecnego ruchu ulicznego w Trojmiescie--%>
<div id="map"></div>
<script>
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: {lat: 54.3778040, lng: 18.6084660}
        });

        var trafficLayer = new google.maps.TrafficLayer();
        trafficLayer.setMap(map);


        var directionDisplay = new google.maps.DirectionRenderer();
        var directionService = new google.maps.DirectionService();

        var pointA = new google.maps.LatLng(54.3778040, 18.6084660);
        var pointB = new google.maps.LatLng(54.355944, 18.660466);

        directionDisplay.setMap(map);

        function calculateRoute() {
            var request = {
                origin: pointA,
                destination: pointB,
                travelMode: 'DRIVING'
            };
            directionService
            route(request, function (result, status) {
                if (status == "OK") {
                    directionDisplay.setDirections(result);
                }
            });
        }

        document.getElementById('get')
        onclick = function () {
            calculateRoute();
        };
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC78bKbjgBajV_NA-SnABUXeI0JDfgPZyA&callback=initMap">
</script>
</body>
</html>