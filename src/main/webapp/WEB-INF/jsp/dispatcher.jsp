<%--
  Created by IntelliJ IDEA.
  User: KW
  Date: 10/30/2017
  Time: 8:03 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Statement" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN"
"http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Patrols</title>
    <style>
        body {
            background-color: #fffff9;
        }

        /*ostylowana tabela oraz czcionki*/
        div.table-title {
            display: block;
            margin: auto;
            max-width: 600px;
            padding: 5px;
        }

        .table-title h3 {
            color: #1b1e24;
            font-size: 40px;
            font-weight: 400;
            font-style: normal;
            font-family: "Roboto", helvetica, arial, sans-serif;
            text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
        }

        .table-fill {
            background: white;
            border-radius: 3px;
            border-collapse: collapse;
            height: 320px;
            margin: auto;
            max-width: 600px;
            padding: 5px;
            width: 100%;
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
        }

        th {
            color: #D5DDE5;
            background: #1b1e24;
            border-bottom: 4px solid #9ea7af;
            border-right: 1px solid #343a45;
            font-size: 23px;
            font-weight: 100;
            padding: 24px;
            text-align: left;
            vertical-align: middle;
        }

        tr {
            border-top: 1px solid #C1C3D1;
            border-bottom-: 1px solid #C1C3D1;
            color: #666B85;
            font-size: 16px;
            font-weight: normal;
            text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);

        }

        /*napis gorny - style*/
        #h3 {
            font-size: 50px;
            font-family: "Roboto", helvetica, arial, sans-serif;
            font-weight: 100;
            line-height: 0.5;
            color: #D5DDE5;;
            background: #1b1e24;
            border-bottom: 4px solid #9ea7af;
            border-right: 1px solid #343a45;
            padding: 16px;
            text-transform: uppercase;
        }

        /*wielkosc mapy oraz cienie*/
        #map {
            margin: auto;
            height: 550px;
            width: 800px;
            border-bottom: 4px solid #9ea7af;
            border-left: 1px solid #343a45;
            border-right: 4px solid #9ea7af;

        }

        /*buttony w tabeli*/
        .simpleButton {
            background-color: whitesmoke;
            border: none;
            color: black;
            width: 100px;
            padding: 10px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 15px;
            transition-duration: 0.4s;
        }

        .simpleButton {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        /*button do dodawania nowego zgloszenia*/
        .Button {
            background-color: whitesmoke;
            color: black;
            width: 120px;
            padding: 10px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
            margin: 4px 2px;
            cursor: pointer;
            margin-left: 580px;
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

        }
    </style>
</head>
<body>
<div id="h3">
    Patrol dispatcher <br/>
    <br/>
</div>
<div id="map">
    <script>

        function initMap() {
            // opisuje w jakim polozeniu ma sie centralizowac mapa - ustawiono obszar Trojmiasta
            var options = {
                zoom: 12,
                center: {lat: 54.3778040, lng: 18.6084660}
            }

            // New map
            var map = new google.maps.Map(document.getElementById('map'), options);

            //Nasluchiwacz klikniecia
            google.maps.event.addListener(map, 'click', function (event) {
                // Dodaje punkt emergency na mape - przyszlosciowo zostanie rozbudowany model o mozliwosc
                //dodawania patroli na mapie Dispatchera i zapisywanie w bazie danych.
                addMarker({coords: event.latLng});
            });

            // Tablica dodanych jednostek patrolujacych
            var markers = [
                {
                    coords: {lat: 54.372108, lng: 18.611082},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 0</h4>'
                },
                {
                    coords: {lat: 54.3778040, lng: 18.6084660},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 1</h4>'
                },
                {
                    coords: {lat: 54.355944, lng: 18.660466},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 2</h4>'
                },
                {
                    coords: {lat: 54.455944, lng: 18.560466},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 2</h4>'
                },

                {
                    coords: {lat: 54.755944, lng: 18.360466},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 2</h4>'
                },
                {
                    coords: {lat: 55.355944, lng: 18.910466},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 2</h4>'
                },
                {
                    coords: {lat: 54.955944, lng: 18.860466},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 2</h4>'
                },
                {
                    coords: {lat: 54.381406, lng: 18.609681},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 2</h4>'
                },
                {
                    coords: {lat: 54.381206, lng: 18.605681},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 2</h4>'
                },

                {
                    coords: {lat: 54.381426, lng: 18.609621},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 2</h4>'
                },
                {
                    coords: {lat: 54.381306, lng: 18.649681},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 2</h4>'
                },
                {
                    coords: {lat: 54.055944, lng: 18.060466},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 2</h4>'
                },
                {
                    coords: {lat: 54.155944, lng: 18.260466},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 2</h4>'
                },

                {
                    coords: {lat: 54.342846, lng: 18.633510},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 3</h4>'
                },
                {
                    coords: {lat: 54.357829, lng: 18.597607},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 4</h4>'
                },

                {
                    coords: {lat: 54.385276, lng: 18.560241},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 4</h4>'
                },
                {
                    coords: {lat: 54.394430, lng: 18.601080},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 4</h4>'
                },
                {
                    coords: {lat: 54.3789510, lng: 18.6191050},
                    content: '<h4>POLICE 0</h4>'
                },
                {
                    coords: {lat: 54.369182, lng: 18.583083},
                    content: '<h4>POLICE 0</h4>'
                },
                {
                    coords: {lat: 54.364074, lng: 18.680414},
                    content: '<h4>POLICE 0</h4>'
                },
                {
                    coords: {lat: 54.406228, lng: 18.607194},
                    content: '<h4>POLICE 0</h4>'
                },
                {
                    coords: {lat: 54.3816370, lng: 18.6118910},
                    content: '<h4>POLICE 1</h4>'
                },

                {
                    coords: {lat: 54.358668, lng: 18.624923},
                    content: '<h4>POLICE 2</h4>'
                },
                {
                    coords: {lat: 54.343712, lng: 54.343712},
                    content: '<h4>POLICE 3</h4>'
                },
                {
                    coords: {lat: 54.3789510, lng: 18.6191050},
                    content: '<h4>POLICE 4</h4>'
                },
                {
                    coords: {lat: 54.407361, lng: 18.577559},
                    content: '<h4>POLICE 5</h4>'
                }, {
                    coords: {lat: 54.417361, lng: 18.617559},
                    content: '<h4>POLICE 5</h4>'
                },
                {
                    coords: {lat: 54.427361, lng: 18.447559},
                    content: '<h4>POLICE 5</h4>'
                },
                {
                    coords: {lat: 54.447361, lng: 18.617559},
                    content: '<h4>POLICE 5</h4>'
                },
                {
                    coords: {lat: 54.417361, lng: 18.497559},
                    content: '<h4>POLICE 5</h4>'
                },
                {
                    coords: {lat: 54.457829, lng: 18.517607},
                    iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    content: '<h4>AMBULANCE 4</h4>'
                }
            ];

            // Petla dodajaca markery
            for (var i = 0; i < markers.length; i++) {
                // dodanie pojedynczego markera
                addMarker(markers[i]);
            }

            // Funkcja dodajaca markery na mapie
            function addMarker(props) {
                var marker = new google.maps.Marker({
                    position: props.coords,
                    map: map,
                });

                // Sprawdza czy istnieja ikonki nieszablonowe
                if (props.iconImage) {
                    // Set icon image
                    marker.setIcon(props.iconImage);
                }

                // Sprawdza kontent
                if (props.content) {
                    var infoWindow = new google.maps.InfoWindow({
                        content: props.content
                    });

                    marker.addListener('click', function () {
                        infoWindow.open(map, marker);
                    });
                }
            }
        }

    </script>
    <%--klucz apiKey googleMaps--%>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC78bKbjgBajV_NA-SnABUXeI0JDfgPZyA&callback=initMap">
    </script>
</div>
<form id="table" method="post">
    <div class="table-title">
        <%--Lista obecnych notyfikacji. Uwaga! Lista zawiera jedynie pozycjie, które nie zostaly jeszcze rozpoczete przez zadna jednostke pomocnicza oraz nie zostaly odwolane.
        Gdy ofiara doda zgloszenie, lista aktualizowana o nowa pozycje--%>
        <%--W przyszlosci dodac dispatchera badz wzorzec observer observable--%>
        <h3>List of current notifications:</h3>
    </div>
    <table class="table-fill">
        <thead>
        <tr>
            <th class="text-left">Latitude</th>
            <th class="text-left">Longitude</th>
            <th class="text-left"></th>

        </tr>
        </thead>
        <tbody class="table-hover">


            <%
                try {
                    Class.forName("com.mysql.jdbc.Driver");
                    String url = "jdbc:mysql://127.0.0.1:3306/myfinalsql";
                    String username = "root";
                    String password = "root";
                    String query = "select * from notification n WHERE n.is_in_progress=FALSE OR n.is_cancelled=FALSE ";
                    Connection conn = DriverManager.getConnection(url, username, password);
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery(query);
                    while (rs.next()) {

            %>
        <tr>
            <td class="text-left"><%=rs.getDouble("latitude") %>
            </td>
            <td class="text-left"><%=rs.getDouble("longitude") %>
            </td>
            <td>
                <input type="button" class="simpleButton" value="Take an action"
                       onclick="window.location.href='http://localhost:8080/actionpanel/?lat=<%=rs.getDouble("latitude") %>&lng=<%=rs.getDouble("longitude") %>'">
                <input type="button" class="simpleButton" value="add details"
                       onclick="window.location.href='http://localhost:8080/addNotificationForm/?lat=<%=rs.getDouble("latitude") %>&lng=<%=rs.getDouble("longitude") %>'">
            </td>
        </tr>
            <%
                }
            %>
    </table>
    <%
            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    %>
</form>
<input type="button" class="Button" value="Add notification"
       onclick="window.location.href='http://localhost:8080/addNotificationForm'">
</body>
</html>




