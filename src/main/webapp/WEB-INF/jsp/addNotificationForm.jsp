<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <style>
        /*formularz dodawania notyfikacji*/
        /*ramki i labele */
        form {
            margin: 0 auto;
            width: 300px;
            padding: 1em;
            border: 1px solid #CCC;
            border-radius: 1em;
        }

        label {
            display: inline-block;
            text-align: right;
            padding-left: 90px;
            margin-left: .5em;
        }

        /*style - czcionki*/
        h2 {
            margin-top: 60px;
            text-align: center;
            font-family: 'Lato Light';
            font-weight: 100;
        }

        input, textarea {
            width: 300px;
            box-sizing: border-box;
            border: 1px solid #999;
            font-family: "Lato Light";
        }

        /*ostylowany button zapisywania formularza*/
        .simpleButton {
            background-color: whitesmoke;
            border: none;
            color: black;
            width: 100px;
            padding: 10px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 15px;
            transition-duration: 0.4s;
        }

        .simpleButton {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

    </style>
</head>
<body>
<input type="button" class="simpleButton" value="go back"
       onclick="window.location.href='http://localhost:8080/dispatcher'">
<div class="form-style-6">
    <h2>Notification form:</h2>
    <form action="/dispatcher" method="post">
        <input type="text" name="name" placeholder="name"/>
        <br>
        <select id="job" name="type">
            <option value="chose">Type of accident</option>
            <optgroup label="">
                <option value="ROAD_ACCIDENT">Road accidents para</option>
                <option value="CRIMES_AND_MURDERS">Crimes and murders</option>
                <option value="MINOR_OFFENCES">Minor offences</option>
                <option value="HEALTH_ACCIDENT">Health accidents</option>
            </optgroup>
        </select>
        <br>
        <textarea name="description" placeholder="description"></textarea>
        <br>
        <%--//${} wstawia dane przeslane przez URL z widoku Dispatcher, jezeli istnieja:--%>

        <input type="text" name="latitude" placeholder="latitude" value=" ${lat}"/>
        <br>
        <input type="text" name="longitude" placeholder="longitude" value=" ${lng}"/>
        <br>
        <input type="text" name="time" placeholder="time to reach the destination (in minutes)"/>
        <br>
        <br>
        <select id="job2" name="isInProgress">
            <option value="choose2">progress</option>
            <optgroup label="">
                <option value="NEW">new</option>
                <option value="IN_PROGRESS">in progress</option>
                <option value="CLOSED">closed</option>
            </optgroup>
        </select>
        <select id="job3" name="isCancelled">
            <option value="choose2">cancelled?</option>
            <optgroup label="">
                <option value="true">yes</option>
                <option value="false">no</option>
            </optgroup>
        </select>
        <%--domyslnie wstawia dzisiejsza date do formularza--%>
        <input type="hidden" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${now}"/>"/>
        <input type="submit" value="Submit" onclick="return foo();">
    </form>
</div>
<%--funkcja wysylajaca komunikat o zapisywaniu notyfikacji do bazy dannych--%>
<script>
    function foo() {
        alert("Notification has been saved!");
        return true;
    }
</script>
</body>
</html>
