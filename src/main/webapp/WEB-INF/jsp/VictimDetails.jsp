<%--
  Created by IntelliJ IDEA.
  User: KW
  Date: 11/5/2017
  Time: 7:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">

    </script>
    <style>
        /*formularz i labele - style*/
        form {
            margin: 0 auto;
            width: 300px;
            padding: 1em;
            border: 1px solid #CCC;
            border-radius: 1em;
        }

        label {
            display: inline-block;
            text-align: right;
            padding-left: 90px;
            margin-left: .5em;
        }
        h2{
            margin-top: 60px;
            text-align: center;
            font-family: 'Lato Light';
            font-weight: 100;
        }

        input, textarea {
            width: 300px;
            box-sizing: border-box;
            border: 1px solid #999;
            font-family: "Lato Light";
        }
        .button {
            padding-left: 90px;
            margin-left: .5em;
        }
        .simpleButton {
            background-color: whitesmoke;
            border: none;
            color: black;
            padding: 10px 15px;
            width:100px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 15px;
            transition-duration: 0.4s;
        }

        .simpleButton {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
        }

    </style>
</head>
<body>
<input type="button" class ="simpleButton" value="go back"
       onclick="window.location.href='http://localhost:8080/'">

<div class="form-style-6">
    <h2>Enter your details:</h2>
    <form action="/" method="post">
        <input type="text" name="name" placeholder="name" />
        <br>
        <input type="text" name="socialSecurityNumber" placeholder="Social Security Number (PESEL)" />
        <br>
        <input type="text" name="address" placeholder="address" />
        <br>
        <input type="text" name="phoneNumber" placeholder="phone number" />
        <br>
        <input type="text" name="latitude" placeholder="latitude" />
        <br>
        <input type="text" name="latitude" placeholder="longitude" />
        <br>
        <input type="submit" value="Submit" onclick="return foo();" />
    </form>
</div>
<script>
    //alert powiadamiasjacy o dodaniu rekordu do bazy danych
    function foo()
    {
        alert("Notification has been saved!");
        return true;
    }
</script>
</body>
</html>
