<%--
  Created by IntelliJ IDEA.
  User: KW
  Date: 10/28/2017
  Time: 10:43 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="userForm" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:useBean id="victim" class="MyFinalProject.model.Victim" scope="session"/>

<html>
<head>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:100' rel='stylesheet' type='text/css'>
    <title>Form</title>
    <style>
        body {
            background-color: black;
        }

        /*Ostylowanie mapy*/
        #map {
            float: right;
            width: 700px;
            height: 400px;
            margin-right: 100px;
            margin-bottom: 40px;
            border-radius: 10px;

        }

        /*czcionka informujaca do czego sluzy przycisk pomocy i co to za strona*/
        #needHelp {
            float: left;
            margin-top: 20px;
            margin-left: 100px;
            font-family: Roboto, sans-serif;
            font-size: 18px;
            font-weight: 100;
            line-height: 1em;
            color: white;
        }

        /*powitalna czcionka*/
        #hello {
            margin-top: 15px;
            margin-left: 350px;
            font-family: Roboto, sans-serif;
            font-size: 80px;
            font-weight: 100;
            line-height: 0.7em;
            color: white;
        }

        fieldset {
            border-radius: 1em;

        }

        .button {
            display: inline-block;
            padding: 15px 25px;
            margin-left: 70px;
            font-size: 24px;
            cursor: pointer;
            text-align: center;
            text-decoration: none;
            outline: none;
            color: #fff;
            background-color: #4CAF50;
            border: none;
            border-radius: 15px;
            box-shadow: 0 9px #999;
        }
/*kolor czcionki pobranej geolokacji*/
        #latitude, #longitude {
            color: white;
        }

        .button:hover {
            background-color: #3e8e41
        }

        .button:active {
            background-color: #3e8e41;
            box-shadow: 0 5px #666;
            transform: translateY(4px);
        }

        .simpleButton {
            background-color: whitesmoke;
            border: none;
            color: black;
            padding: 10px 20px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 15px;
            transition-duration: 0.4s;
        }

        .simpleButton {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

    </style>
</head>
<body>
<div id="hello">
    Emergency system
</div>
<br/>
<br/>
<br/>
<div id="needHelp">
    Welcome in UberFast Emergency System!
    <br/> <br/>
    <br/>
    Push the button, in case of emergency.
    <br/>
    <br/>
    <%--po kliknieciu przycisku odpala sie javaScriptowa maszyna, ktora pobiera obecna lokalizacje uzytkownika--%>
    <input type="button" class="button alert" value="Need help!"
           onclick="javascript:showlocation()"/><br/>
    <br>
    <br>
    <%--Tutaj umieszczona jest pobrana wartosc--%>
    <fieldset>
        Latitude: <span id="latitude"></span><br/>
        Longitude: <span id="longitude"></span>
    </fieldset>
    <br/>
    <br/>
    <hr>
    Enter your additional details:
    <br/>
    <br/>
<%--Mozliwe sa dodanie opcjonalne danych o ofiarze. W przyszlosci ofiare bedzie mozna powiazac relacja one to many z notyfikacja. Obecnie ma charakter jedynie informacyjny--%>
    <input type="button" class="simpleButton" value="add details"
           onclick="window.location.href='http://localhost:8080/victimDetails'">
</div>

<div id="map"></div>
<script>

    var map, infoWindow;
//Dodajemy mape do wygladu.
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 54.3778040, lng: 18.6084660},
            zoom: 16
        });
        infoWindow = new google.maps.InfoWindow;

//        Nawiguje geolokalizacje, przypisuje punkty lat i lnguzywajac funkcji - getCurrent Position -wyszukujaca punkt na mapie
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                infoWindow.setPosition(pos);
                infoWindow.setContent('Location found.');
                infoWindow.open(map);
                map.setCenter(pos);
            }, function () {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Jezeli nie moze odnalezc punktu na mapie - rzuca bledem
            handleLocationError(false, infoWindow, map.getCenter());
        }
    }


    function showlocation() {
        //Polazuje poycje jako jeden parametr - position - wykorzystuje funkcje callback
        navigator.geolocation.getCurrentPosition(callback);

    }
//funkcja wyszukuje punkty na mapie, nastepnie zapisuje je w bazie danych jako simple notification - dwa punkty.
    function callback(position) {
        document.getElementById('latitude').innerHTML = position.coords.latitude;
        document.getElementById('longitude').innerHTML = position.coords.longitude;
        //alert - show when you push the button "need help"
        alert("Don't worry, help is on the way!");

        fetch('http://localhost:8080/simpleNotifications/simpleNotification', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'

            },
            body: JSON.stringify({
                'latitude': position.coords.latitude,
                'longitude': position.coords.longitude
            })
        })

    }
    //Obsluguje bledy, gdy problemy z serwerem lub przegladarka
    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
    }
</script>
<%--apikey googleMaps--%>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC78bKbjgBajV_NA-SnABUXeI0JDfgPZyA&callback=initMap">
</script>
</body>
</html>
